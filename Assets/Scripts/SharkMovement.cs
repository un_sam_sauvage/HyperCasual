﻿using UnityEngine;

public class SharkMovement : MonoBehaviour, IPooledObject
{
    private float _countdownToSetActive;

    private float _speed;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * _speed);
        _countdownToSetActive -= Time.deltaTime;
        if (_countdownToSetActive <= 0)
        {
            gameObject.SetActive(false);
        }
    }

    public void OnObjectSpawn(float speed)
    {
        _countdownToSetActive = 5f;
        _speed = speed;
    }
}