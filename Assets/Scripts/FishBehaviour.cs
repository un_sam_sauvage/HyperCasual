﻿using UnityEngine;

public class FishBehaviour : MonoBehaviour
{
    private GameObject _player;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerNextToFish())
        {
            _player.GetComponent<PlayerInfo>().GetFish(gameObject);
        }
    }

    private bool PlayerNextToFish()
    {
        Vector3 playerPos = _player.transform.position;
        if (transform.position + Vector3.back == playerPos)
        {
            return true;
        }
        else if (transform.position + Vector3.forward == playerPos)
        {
            return true;
        }
        else if (transform.position + Vector3.left == playerPos)
        {
            return true;
        }
        else if (transform.position + Vector3.right == playerPos)
        {
            return true;
        }
        return false;
    }
}