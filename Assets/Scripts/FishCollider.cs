﻿using UnityEngine;

public class FishCollider : MonoBehaviour
{
    private GameObject _player;

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Shark"))
        {
            _player.GetComponent<PlayerInfo>().isFishTouched = true;
            other.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
