﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject panelWin;
    public GameObject panelLose;
    
    public static GameManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void EndLevel()
    {
        panelWin.SetActive(true);
        Debug.Log("vous avez fini le niveau");
    }

    public void GameOver()
    {
        panelLose.SetActive(true);
    }
}
