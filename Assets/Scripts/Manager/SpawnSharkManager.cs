﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSharkManager : MonoBehaviour
{


    private ObjectPooler _instance;

    public float repeatSpawnRate;
    public float firstTimeSpawn;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnShark", firstTimeSpawn,repeatSpawnRate);
        _instance = ObjectPooler.instance;
    }

    void SpawnShark()
    {
        _instance.SpawnFromPool("Shark", transform.position, transform.rotation,speed);
    }
}
