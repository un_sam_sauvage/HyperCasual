﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    public int fishGet;
    public int fishToGet;

    public bool isFishTouched;
    
    public List<Transform> fishPos;
    private List<GameObject> _fishGetGO = new List<GameObject>();
    public void GetFish(GameObject fish)
    {
        _fishGetGO.Add(fish);
        fish.transform.position = fishPos[fishPos.Count - fishToGet].position;
        fish.transform.SetParent(fishPos[fishPos.Count - fishToGet]);
        fish.GetComponent<FishBehaviour>().enabled = false;
        fish.GetComponent<FishCollider>().enabled = true;
        fishGet++;
        fishToGet--;
        //TODO changer la condition de victoire
        if (fishToGet <= 0)
        {
            GameManager.instance.EndLevel();
        }
    }

    public void LoseFish()
    {
        fishGet--;
        isFishTouched = false;
        if (fishGet <0)
        {
            GameManager.instance.GameOver();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.CompareTag("Shark")&& !isFishTouched && _fishGetGO.Count >0)
        {
            other.gameObject.SetActive(false);
            for (int i = _fishGetGO.Count-1; i > -1; i--)
            {
                if (_fishGetGO[i].activeInHierarchy)
                {
                    Debug.Log("je perds une vie parce qu'un requin m'a touché");
                    _fishGetGO[i].SetActive(false);
                    LoseFish();
                    return;
                }
            }
            Debug.Log("je me lis quand même");
            LoseFish();
        }
        else if (other.gameObject.CompareTag("Shark"))
        {
            other.gameObject.SetActive(false);
            LoseFish();
        }
    }
}
