﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public void GoForward()
    {
        transform.position += Vector3.forward;
    }

    public void GoLeft()
    {
        transform.position += Vector3.left;
    }

    public void GoRight()
    {
        transform.position += Vector3.right;
    }
}
